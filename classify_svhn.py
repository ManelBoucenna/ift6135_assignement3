#!/usr/bin/env python
import torch
import torchvision.datasets
import torchvision.transforms as transforms
from torch import nn
# from torch.nn.modules import upsampling
# from torch.functional import F
from torch.optim import Adam
from torch.utils.data import dataset
from tqdm import tqdm

cuda = torch.cuda.is_available()

device = torch.device('cuda:0' if cuda else 'cpu')
tqdm.write('CUDA is not available!' if not cuda else 'CUDA is available!')
tqdm.write('')

image_transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((.5, .5, .5),
                         (.5, .5, .5))
])


def get_data_loader(dataset_location, batch_size):
    trainvalid = torchvision.datasets.SVHN(
        dataset_location, split='train',
        download=True,
        transform=image_transform
    )

    trainset_size = int(len(trainvalid) * 0.9)
    trainset, validset = dataset.random_split(
        trainvalid,
        [trainset_size, len(trainvalid) - trainset_size]
    )

    trainloader = torch.utils.data.DataLoader(
        trainset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2
    )

    validloader = torch.utils.data.DataLoader(
        validset,
        batch_size=batch_size,
    )

    testloader = torch.utils.data.DataLoader(
        torchvision.datasets.SVHN(
            dataset_location, split='test',
            download=True,
            transform=image_transform
        ),
        batch_size=batch_size,
    )

    return trainloader, validloader, testloader


def to_var(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return torch.autograd.Variable(x)


class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()
        self.convs = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),  # 16

            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(128, 256, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),  # 8

            nn.Conv2d(256, 512, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(512, 1024, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),  # 4

            # nn.Conv2d(1024, 1024, kernel_size=3, padding=1),
        )
        self.linear = nn.Sequential(
            # nn.ReLU(),
            # nn.Dropout(0.5),
            nn.Linear(4*4*1024, 10),
        )

    def reparam(self, mu, log_var):
        esp = to_var(torch.randn(*mu.size()))
        std = torch.exp(log_var / 2)
        return mu + std * esp

    def encode(self, x_in):
        x_in = self.convs(x_in)
        x_in = self.linear(x_in.view(x_in.size(0), -1))
        # mu, log_var = torch.chunk(x_in, 2, dim=1)
        return x_in

    def forward(self, x_in):
        # mu, log_var = self.encode(x_in)
        # sample = self.reparam(mu, log_var)
        return self.encode(x_in)


class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.linear = nn.Sequential(
            nn.Linear(100, 256),
            nn.ELU(),
        )

        self.convs = nn.Sequential(
            nn.ConvTranspose2d(16, 12, kernel_size=2, stride=2),  # 8
            nn.ELU(),
            nn.ConvTranspose2d(12, 8, kernel_size=2, stride=2),  # 16
            nn.ELU(),
            nn.ConvTranspose2d(8, 3, kernel_size=2, stride=2),  # 32
            nn.Sigmoid()
        )

    def forward(self, z_in):
        z_in = self.linear(z_in)
        z_in = self.convs(z_in.view(-1, 16, 4, 4))
        return z_in


class ELBOLoss(nn.Module):
    def __init__(self):
        super(ELBOLoss, self).__init__()
        self.recons_loss = nn.BCELoss(reduction='sum')

    def forward(self, reconstruction, x, mu, log_var):
        # reconstruction = reconstruction.view(-1, 3072)
        # x = x.view(-1, 3072)
        loss = -self.recons_loss(reconstruction, x)
        KL_loss = 0.5 * torch.sum(-1 - log_var + mu ** 2 + log_var.exp())
        return -(loss - KL_loss)


def iterate(epoch, models, criterion, optimizer, loaders, mode='train'):
    # encoder = models["encoder"]
    # decoder = models["decoder"]
    if mode == 'train':
        for model in models.values():
            model.train()
    elif mode == 'valid' or mode == 'test':
        for model in models.values():
            model.eval()

    run_loss = 0.
    num_images = 0

    criterion = nn.CrossEntropyLoss()

    monitor = tqdm(loaders[mode], desc=mode)
    for images, labels in monitor:
        images, labels = images.to(device), labels.to(device)

        z = models['encoder'](images)
        # out = models['decoder'](z)

        # loss = criterion(out, images, mu, log_var)
        loss = criterion(z, labels)

        run_loss += loss.item() * images.shape[0]
        num_images += images.shape[0]

        if mode == 'train':
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        monitor.set_postfix(epoch=epoch, loss=run_loss / num_images)

    return run_loss / num_images


if __name__ == "__main__":

    # train, valid, test = get_data_loader('svhn', batch_size=32)
    train, valid, test = get_data_loader("/export/livia/data/CommonData/cows_data/svhn", batch_size=16)
    loaders = {'train': train, 'valid': valid, 'test': test}
    models = {'encoder': Encoder().to(device), 'decoder': Generator().to(device)}
    epochs = 20
    learning_rate = 1e-5

    criterion = ELBOLoss()
    optimizer = Adam(models['encoder'].parameters(), lr=learning_rate)

    train_losses = []
    for epoch in range(epochs):
        train_loss = iterate(epoch, models, criterion, optimizer, loaders, mode='train')
        train_losses.append(train_loss)
    
    #Saving the model parameters   
    #torch.save(models.state_dict(), PATH)
    
    #Saving the entire model
    #torch.save(models, PATH)
 
#Reloading the model parameters 
#model = models(*args, **kwargs)
#model.load_state_dict(torch.load(PATH))

#Reloading the entire model
#model = torch.load(PATH)
