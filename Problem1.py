#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 13:20:15 2019

@author: chin-weihuang
"""

from __future__ import print_function

import math
import os

import torch
import torch.nn as nn
from torch.autograd import Variable
from tqdm import tqdm

from samplers import *

cuda = torch.cuda.is_available()

device = torch.device('cuda:0' if cuda else 'cpu')
tqdm.write('CUDA is not available!' if not cuda else 'CUDA is available!')
tqdm.write('')


# Jensen Shannon Divergence Loss Function
class JSDLoss(nn.Module):
    def __init__(self):
        super(JSDLoss, self).__init__()

    def forward(self, d_x, d_y):
        return -(math.log(2.0) + 0.5 * (torch.mean(torch.log(d_x)) + torch.mean(torch.log(1.0 - d_y))))


# Wasserstein Distance Loss Function
class WDLoss(nn.Module):
    def __init__(self, _lambda):
        super(WDLoss, self).__init__()
        self._lambda = _lambda

    def forward(self, t_x, t_y, t_z):
        return -(torch.mean(t_x) - torch.mean(t_y) - self._lambda * torch.mean((torch.norm(t_z, dim=1) - 1).pow(2)))


# NNL Loss Function
class NLLLoss(nn.Module):
    def __init__(self):
        super(NLLLoss, self).__init__()

    def forward(self, d_f0, d_f1):
        return -(torch.mean(torch.log(d_f1)) + torch.mean(torch.log(1.0 - d_f0)))


# MLP for Discriminator
class Discriminator(nn.Module):
    def __init__(self, input_dim, hidden_size, activation_func):
        super(Discriminator, self).__init__()
        layers = []
        hidden_size.append(1)  # Output of discriminator
        for hidden in hidden_size:
            layers += [nn.Linear(input_dim, hidden)]
            layers += [nn.Tanh()]
            input_dim = hidden
        del layers[-1]
        self.disc = nn.Sequential(*layers)
        self.last_activation = activation_func

    def reset_parameters(self):
        for layer in self.disc:
            if isinstance(layer, nn.Linear):
                layer.reset_parameters()

    def forward(self, x):
        return self.last_activation(self.disc(x))


def iterate(epoch, model, criterion, optimizer, total_samples, x_sampler, y_sampler, theta, mode='train'):
    if mode == 'train':
        model.train()
    elif mode == 'valid':
        model.eval()

    run_loss = 0.
    num_samples = 0

    monitor = tqdm(range(total_samples), desc=mode)
    for _ in monitor:
        x_tensor = torch.Tensor(next(x_sampler)).to(device)
        y_tensor = torch.Tensor(next(y_sampler)).to(device)

        # Note that d_x is distribution f0 and d_y is distribution f1 in the case of question 1.4
        d_x = model(x_tensor)
        d_y = model(y_tensor)

        if isinstance(criterion, WDLoss):
            a_sampler = iter(distribution2(batch_size=batch_size))
            a_tensor = torch.Tensor(next(a_sampler)).to(device)
            z_tensor = a_tensor * x_tensor + (1 - a_tensor) * y_tensor
            # z_sampler = iter(distribution1(0, batch_size=batch_size))
            z_variable = Variable(z_tensor, requires_grad=True).to(device)
            d_z = model(z_variable)
            gradients = torch.autograd.grad(outputs=d_z, inputs=z_variable, grad_outputs=torch.ones(d_z.size()).cuda(),
                                            create_graph=True, retain_graph=True)[0]
            loss = criterion(d_x, d_y, gradients)
        else:
            loss = criterion(d_x, d_y)

        run_loss += loss.item() * x_tensor.shape[0]
        num_samples += x_tensor.shape[0]

        if mode == 'train':
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        monitor.set_postfix(epoch=epoch, loss=run_loss / num_samples, theta=theta, bs=x_tensor.shape[0])

    return -run_loss / num_samples


N = lambda x: np.exp(-x ** 2 / 2.) / ((2 * np.pi) ** 0.5)


def estimator(model, xx):
    model.eval()
    with torch.no_grad():
        d_star = model(torch.from_numpy(xx).to(device).float().view(-1, 1))
    return (torch.from_numpy(N(xx)).to(device).float().view(-1, 1) * d_star) / (1.0 - d_star)


def log_results(file_name, losses):
    os.makedirs(os.path.dirname(file_name), exist_ok=True)
    file = open(file_name, 'w+')
    theta = -1.0
    for loss in losses:
        file.write('{:.2f},'.format(theta))
        file.write('{:.5f},'.format(loss))
        file.write('\n')
        theta += 0.1
    file.close()


if __name__ == '__main__':
    # Question 1

    # Configuration
    learning_rate = 1e-3
    batch_size = 512
    hidden_size = [512, 512]

    total_samples = 10240
    epochs = 1

    thetas = np.arange(-1, 1.1, 0.1)

    # Init model
    activation = {"jsd": nn.Sigmoid(), "wd": nn.Tanh()}
    model = Discriminator(input_dim=2, hidden_size=hidden_size, activation_func=activation["jsd"])
    criterion = JSDLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    # CUDA
    model = model.to(device)

    # Question 1

    jsd_losses = []
    for idx, theta in enumerate(thetas):

        model.reset_parameters()
        optimizer.zero_grad()

        # Samplers
        iter_dist_x = iter(distribution1(0, batch_size=batch_size))
        # Create a list of distribution from theta=-1 to +1
        iter_dist_y = iter(distribution1(theta, batch_size=batch_size))

        # jsd_losses.append([])
        for epoch in range(epochs):
            train_loss = iterate(epoch, model, criterion, optimizer, total_samples, iter_dist_x, iter_dist_y, theta,
                                 mode='train')
            with torch.no_grad():
                valid_loss = iterate(epoch, model, criterion, optimizer, 32, iter_dist_x, iter_dist_y, theta,
                                     mode='valid')
            jsd_losses.append(valid_loss)

    log_results('jsd_valid_loss.csv', jsd_losses)

    # Question 2

    model = Discriminator(input_dim=2, hidden_size=hidden_size, activation_func=activation["wd"])
    criterion = WDLoss(10)
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    # CUDA
    model = model.to(device)
    # Update activation for WD loss
    # model.last_activation = activation["wd"]

    wd_losses = []
    for idx, theta in enumerate(thetas):

        model.reset_parameters()
        optimizer.zero_grad()

        # Samplers
        iter_dist_x = iter(distribution1(0, batch_size=batch_size))
        # Create a list of distribution from theta=-1 to +1
        iter_dist_y = iter(distribution1(theta, batch_size=batch_size))

        # wd_losses.append([])
        for epoch in range(epochs):
            train_loss = iterate(epoch, model, criterion, optimizer, total_samples, iter_dist_x, iter_dist_y, theta,
                                 mode='train')
            valid_loss = iterate(epoch, model, criterion, optimizer, 32, iter_dist_x, iter_dist_y, theta,
                                 mode='valid')
            wd_losses.append(valid_loss)

    log_results('wd_valid_loss.csv', wd_losses)

    # Question 3

    model = Discriminator(input_dim=1, hidden_size=hidden_size, activation_func=activation["jsd"])
    criterion = NLLLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    # CUDA
    model = model.to(device)
    # model.last_activation = activation["jsd"]

    nll_losses = []

    iter_dist_f0 = iter(distribution3(batch_size=batch_size))
    iter_dist_f1 = iter(distribution4(batch_size=batch_size))

    for epoch in range(epochs):
        train_loss = iterate(epoch, model, criterion, optimizer, total_samples, iter_dist_f0, iter_dist_f1, 0,
                             mode='train')

    xx = np.linspace(-5, 5, 1000)
    np.savetxt('points.csv', xx, fmt="%0.3f", delimiter=',')
    disc_output = model(torch.from_numpy(xx).to(device).float().view(-1, 1))
    np.savetxt('disc_output.csv', disc_output.cpu().detach().numpy(), fmt="%0.3f", delimiter=',')

    est_f1 = estimator(model, xx)
    est_f1_numpy = est_f1.cpu().detach().numpy()

    tqdm.write('Saving estimate...')
    np.savetxt('est.csv', est_f1_numpy, fmt="%0.3f", delimiter=',')

    f = lambda x: torch.tanh(x * 2 + 1) + x * 0.75
    d = lambda x: (1 - torch.tanh(x * 2 + 1) ** 2) * 2 + 0.75

    np.savetxt('real.csv', d(torch.from_numpy(xx)).numpy()**(-1)*N(xx), fmt="%0.3f", delimiter=',')
    np.savetxt('real_x_axis.csv', f(torch.from_numpy(xx)).numpy(), fmt="%0.3f", delimiter=',')